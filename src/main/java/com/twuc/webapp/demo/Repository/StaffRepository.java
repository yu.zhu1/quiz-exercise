package com.twuc.webapp.demo.Repository;

import com.twuc.webapp.demo.Entity.Staff;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StaffRepository extends JpaRepository<Staff, Integer> {
}
