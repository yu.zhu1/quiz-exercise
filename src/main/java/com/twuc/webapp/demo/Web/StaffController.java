package com.twuc.webapp.demo.Web;

import com.twuc.webapp.demo.Entity.Staff;
import com.twuc.webapp.demo.Repository.StaffRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StaffController {

    private final StaffRepository staffRepository;

    public StaffController(StaffRepository staffRepository) {
        this.staffRepository = staffRepository;
    }

    @GetMapping("/api/staffs/{id}")
    ResponseEntity getStaff(@PathVariable int id) {
        Staff staff = staffRepository.findById(id).orElse(null);
        return ResponseEntity.ok().body(staff);
    }


}
